#define SDL_MAIN_HANDLED
#include "include/screen.h"
#include "include/inputManager.h"
#include "include/Scenes/SceneFactory.h"
#include "Include/Scenes/IScene.h"
#include "FmodIntegration.h"

int main()
{
	InitializeSoundEngine();
	
	Screen* gameScreen = Screen::GetInstance();
	gameScreen->Initialize(1280, 720);
	bool quit = false;
	SDL_Event event;
	SceneFactory* sceneFactoryInst = SceneFactory::GetInstance();
	
	sceneFactoryInst->SwapScenes(Scenes::MAIN_MENU);

	while (!quit)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				quit = true;
			}

			sceneFactoryInst->currentScene->HandleInput();
		}

		sceneFactoryInst->currentScene->Update(1.0/60.0);
		gameScreen->ClearScreen();
		UpdateSoundEngine();
		sceneFactoryInst->currentScene->Draw();
		gameScreen->RenderScreen();
		
	}

	delete sceneFactoryInst;
	delete gameScreen;
	TerminateSoundEngine();

	return 0;
}
