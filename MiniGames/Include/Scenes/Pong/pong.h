﻿#pragma once

#include "Ball.h"
#include "LTexture.h"
#include "Paddle.h"
#include "Scenes/IScene.h"
#include "Scenes/GameState.h"

class Pong : public IScene
{
public:
	//Scene methods
	~Pong() override = default;
	void InitializeScene() override;
	void HandleInput() override;
	void Update(const double deltaTime) override;
	void Draw() override;
	void CloseScene() override;

	//Pong Methods
	GameState GetGameState() const;
	void SetGameState(const GameState state);
	void ResetStartTimer();

	int playerScoreLeft{0};
	int playerScoreRight{0};
private:
	GameState gameState{GameState::StartGame};
	Paddle paddleLeft{PaddlePosition::LEFT};
	Paddle paddleRight{PaddlePosition::RIGHT};
	Ball ball{};
	LTexture startGameImage{};
	SDL_TimerID startTimer{0};

};

inline void Pong::ResetStartTimer()
{
	startTimer = 0;
}
