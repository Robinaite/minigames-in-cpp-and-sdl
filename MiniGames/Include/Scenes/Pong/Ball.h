﻿#pragma once
#include <SDL_rect.h>

#include "LTexture.h"

class Pong;
class Paddle;

class Ball
{
public:
	Ball() = default;
	void Initialize(Pong *gameMode);
	void Update(const double deltaTime);
	void Draw();
	void CheckCollisionWithPaddle(Paddle &paddle);
	void RandomizeStartingVelocity();
	void PointScored();
	void SetScoreText();
private:
	LTexture ballImg;
	LTexture scoreText;
	SDL_Rect rect;
	int xVel{};
	int yVel{};
	double xPos{};
	double yPos{};
	Pong* gameMode;
};
