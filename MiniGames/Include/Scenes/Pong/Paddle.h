﻿#pragma once
#include <SDL.h>

enum class PaddlePosition { LEFT,RIGHT };
enum class Direction { NONE,UP,DOWN };

class Paddle
{
public:
	Paddle(PaddlePosition pos);
	void InitializePaddle(const int distFromCenter);
	void HandleInput();
	void Update(const double deltaTime);
	void Draw();
	PaddlePosition GetPaddlePosition() const;
	const SDL_Rect& GetPaddleRect();
private:
	SDL_Rect rect{};
	PaddlePosition paddlePos;
	Direction direction{};
	bool multiKeyPress{false};
	double yPos{};
};
