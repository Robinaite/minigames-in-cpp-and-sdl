﻿#pragma once
enum class GameState
{
	StartGame,
	Playing,
	EndGame
};