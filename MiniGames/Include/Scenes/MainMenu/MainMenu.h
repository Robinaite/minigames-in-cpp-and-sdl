﻿#pragma once

#include "LTexture.h"
#include "Scenes/IScene.h"
#include "screen.h"


class MainMenu : public IScene
{
public:
	~MainMenu() = default;
	void InitializeScene() override;
	void HandleInput() override;
	void Update(const double deltaTime) override;
	void Draw() override;
	void CloseScene() override;
private:
	LTexture mainMenuLogo{};
};
