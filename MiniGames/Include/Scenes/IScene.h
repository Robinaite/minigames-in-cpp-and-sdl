﻿#pragma once

#include "screen.h"

class IScene
{
public:
		
	virtual ~IScene() = default;
	virtual void InitializeScene() =0;
	virtual void HandleInput()=0;
	virtual void Update(const double deltaTime)=0;
	virtual void Draw()=0;
	virtual void CloseScene()=0;
};
