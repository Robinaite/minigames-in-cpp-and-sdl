﻿#pragma once

enum class Scenes
{
	NONE,
	MAIN_MENU,
	PONG
};

class SceneFactory
{
private:
	static inline SceneFactory *ptrInstance;
	SceneFactory() = default;
	class IScene* CreateScene(const Scenes scene);
	Scenes currentSceneID{Scenes::NONE};
public:
	class IScene* currentScene = nullptr;
	
	static inline SceneFactory *GetInstance()
	{
		if(!ptrInstance)
		{
			ptrInstance = new SceneFactory;
		}
		return ptrInstance;
	}
	void SwapScenes(Scenes sceneToSwapTo);
};
