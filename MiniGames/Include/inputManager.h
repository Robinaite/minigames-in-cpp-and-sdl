﻿#pragma once
#include <SDL.h>

class Input
{
public:

	enum class Keys : int
	{
		UP = SDL_SCANCODE_UP,
		DOWN = SDL_SCANCODE_DOWN,
		P = SDL_SCANCODE_P,
		W = SDL_SCANCODE_W,
		S = SDL_SCANCODE_S,
		ENTER = SDL_SCANCODE_RETURN,
		ESC = SDL_SCANCODE_ESCAPE,
	};
	
	Input() = delete;
	~Input() = delete;
	static bool IsPressed(const Keys key);
private:
	
};