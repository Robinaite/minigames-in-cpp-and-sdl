#pragma once

#include <SDL.h>

class Screen
{
public:
	static inline Screen *GetInstance()
	{
		if(!ptrInstance)
		{
			ptrInstance = new Screen;
		}
		return ptrInstance;
	}
	
	~Screen();
	bool Initialize(const int screenWidth,const int screenHeight);
	void SetWindowTitle(const char* sceneTitle);
	void ClearScreen();
	void RenderScreen();
	void Close();
	int GetWidth() const;
	int GetHeight() const;
	int GetWidthCenter() const;
	int GetHeightCenter() const;
	void RenderFillRect(SDL_Rect &rect,Uint8 red, Uint8 green, Uint8 blue,Uint8 alpha = 0xFF);
	
private:
	Screen() = default;
	static inline Screen *ptrInstance;
	SDL_Window* window = nullptr;
	SDL_Renderer* renderer = nullptr;
	int height{};
	int width{};
};