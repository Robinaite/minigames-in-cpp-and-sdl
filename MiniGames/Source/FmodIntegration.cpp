﻿#include "FmodIntegration.h"

#include <cstdio>
#include "fmod_studio.hpp"
#include "fmod_errors.h"

static FMOD::Studio::System* system = nullptr;

void InitializeSoundEngine()
{
	FMOD_RESULT result;
	
	result = FMOD::Studio::System::create(&system); // Create the Studio System object.
	if (result != FMOD_OK)
	{
	    printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
	    return;
	}

	// Initialize FMOD Studio, which will also initialize FMOD Core
	result = system->initialize(512, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, 0);
	if (result != FMOD_OK)
	{
	    printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		return;
	}
	
}

void UpdateSoundEngine()
{
	const FMOD_RESULT result = system->update();
	if(result != FMOD_OK)
	{
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
	}
}

void TerminateSoundEngine()
{
	const FMOD_RESULT result = system->release();
	if(result != FMOD_OK)
	{
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
	}
}



