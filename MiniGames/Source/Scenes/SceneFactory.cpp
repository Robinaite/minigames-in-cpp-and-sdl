﻿#include "Scenes/SceneFactory.h"

#include "Scenes/MainMenu/MainMenu.h"
#include "Scenes/Pong/pong.h"

IScene* SceneFactory::CreateScene(const Scenes scene)
{
	switch (scene) {
		case Scenes::MAIN_MENU:
			return new MainMenu{};
			break;
		case Scenes::PONG:
			return new Pong{};
			break;
		default:
			return nullptr;
	}
}

void SceneFactory::SwapScenes(Scenes sceneToSwapTo)
{
	if (currentSceneID == sceneToSwapTo)
	{
		return;
	}
	currentSceneID = sceneToSwapTo;
	if (currentScene)
	{
		currentScene->CloseScene();
		delete currentScene; 
		currentScene = SceneFactory::CreateScene(sceneToSwapTo);
	}
	else
	{
		currentScene = SceneFactory::CreateScene(sceneToSwapTo);
	}
	currentScene->InitializeScene();
}
