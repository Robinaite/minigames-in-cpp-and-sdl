﻿#include "Scenes/MainMenu/MainMenu.h"
#include "inputManager.h"
#include "Scenes/SceneFactory.h"
#include <iostream>

void MainMenu::InitializeScene()
{
	Screen::GetInstance()->SetWindowTitle("Main Menu!");
	mainMenuLogo.LoadFromFile("images/mainMenu.png");
}

void MainMenu::HandleInput()
{
	if(Input::IsPressed(Input::Keys::P))
	{
		SceneFactory::GetInstance()->SwapScenes(Scenes::PONG);
	}
}

void MainMenu::Update(const double deltaTime)
{
}

void MainMenu::Draw()
{
	mainMenuLogo.Render((Screen::GetInstance()->GetWidth() - mainMenuLogo.GetWidth()) /2,(Screen::GetInstance()->GetHeight() - mainMenuLogo.GetHeight()) /2);
}

void MainMenu::CloseScene()
{
	mainMenuLogo.Free();
}
