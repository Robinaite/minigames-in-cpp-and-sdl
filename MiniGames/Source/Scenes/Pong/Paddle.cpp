﻿#include "Scenes/Pong/Paddle.h"


#include "inputManager.h"
#include "screen.h"

constexpr auto PADDLE_WIDTH = 35;
constexpr auto PADDLE_HEIGHT = 200;
constexpr auto PADDLE_VELOCITY = 7.0;

Paddle::Paddle(PaddlePosition pos)
{
	paddlePos = pos;
	
}

void Paddle::InitializePaddle(const int distFromCenter)
{
	int widthCenter = Screen::GetInstance()->GetWidthCenter();
	if(paddlePos == PaddlePosition::LEFT)
	{
		rect.x = widthCenter - distFromCenter;
	} else
	{
		rect.x = widthCenter + distFromCenter;
	}
	rect.y = Screen::GetInstance()->GetHeightCenter() - PADDLE_HEIGHT/2;
	rect.w = PADDLE_WIDTH;
	rect.h = PADDLE_HEIGHT;
	yPos = rect.y;
}

void Paddle::HandleInput()
{
	bool inputUp{false};
	bool inputDown{false};
	if(paddlePos == PaddlePosition::LEFT)
	{
		 inputUp = Input::IsPressed(Input::Keys::W);
		 inputDown = Input::IsPressed(Input::Keys::S);
	} else
	{
		inputUp = Input::IsPressed(Input::Keys::UP);
		inputDown = Input::IsPressed(Input::Keys::DOWN);
	}
	
	
	if(inputUp && inputDown)
	{
		if(!multiKeyPress)
		{
			multiKeyPress = true;
			if(direction == Direction::UP)
			{
				direction = Direction::DOWN;
			} else if(direction == Direction::DOWN)
			{
				direction = Direction::UP;
			}
		}
		
	} else if(inputUp)
	{
		direction = Direction::UP;
	} else if(inputDown)
	{
		direction = Direction::DOWN;
	}
	if(multiKeyPress)
	{
		if(!inputUp)
		{
			direction = Direction::DOWN;
			multiKeyPress = false;
		} else if(!inputDown)
		{
			direction = Direction::UP;
			multiKeyPress = false;
		}
	}
	
	if((direction == Direction::UP && !inputUp) || (direction == Direction::DOWN && !inputDown))
	{
		multiKeyPress = false;
		direction = Direction::NONE;
	} 
}

void Paddle::Update(const double deltaTime)
{
	if(direction == Direction::NONE)
	{
		return;
	}

	const double finalVel = PADDLE_VELOCITY * deltaTime;
	if(direction == Direction::UP)
	{
		const double nextPos = yPos - finalVel;
		if(nextPos < 0 || nextPos + rect.h > Screen::GetInstance()->GetHeight())
		{
			return;
		}
		yPos = nextPos;
	} else if(direction == Direction::DOWN)
	{
		const double nextPos = yPos + finalVel;
		if(nextPos < 0 || nextPos + rect.h > Screen::GetInstance()->GetHeight())
		{
			return;
		}
		yPos = nextPos;
	}
	rect.y = yPos;
}

void Paddle::Draw()
{
	Screen::GetInstance()->RenderFillRect(rect,0x00,0x00,0x00);
}

PaddlePosition Paddle::GetPaddlePosition() const
{
	return paddlePos;
}

const SDL_Rect& Paddle::GetPaddleRect()
{
	return rect;
}
