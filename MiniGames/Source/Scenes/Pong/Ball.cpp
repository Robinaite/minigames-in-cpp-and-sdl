﻿#include <random>
#include "Scenes/Pong/Ball.h"
#include "Scenes/Pong/Paddle.h"
#include "screen.h"
#include "Scenes/Pong/pong.h"

constexpr int BALL_WIDTH_HEIGHT = 50;
constexpr  int BALL_SPEED = 10;

void Ball::SetScoreText()
{
	std::string score{};
	score += std::to_string(gameMode->playerScoreLeft);
	score += " - ";
	score += std::to_string(gameMode->playerScoreRight);
	
	
	scoreText.LoadFromRenderedText(score,SDL_Color{0x00,0x00,0x00,0xFF});
	scoreText.SetWidthAndHeight(scoreText.GetWidth()*3,scoreText.GetHeight()*3);
}

void Ball::Initialize(Pong *gameMode)
{
	this->gameMode = gameMode;
	ballImg.LoadFromFile("images/ballPong.png");
	rect.w = BALL_WIDTH_HEIGHT;
	rect.h = BALL_WIDTH_HEIGHT;
	rect.x = Screen::GetInstance()->GetWidthCenter();
	rect.y = Screen::GetInstance()->GetHeightCenter();
	xPos = rect.x;
	yPos = rect.y;

	ballImg.SetWidthAndHeight(BALL_WIDTH_HEIGHT,BALL_WIDTH_HEIGHT);
	RandomizeStartingVelocity();

	SetScoreText();
}

void Ball::PointScored()
{
	SetScoreText();
	xPos = Screen::GetInstance()->GetWidthCenter();
	yPos = Screen::GetInstance()->GetHeightCenter();
	rect.x = xPos;
	rect.y = yPos;
	RandomizeStartingVelocity();
}

void Ball::Update(const double deltaTime)
{
	if(gameMode->GetGameState() != GameState::Playing)
	{
		return;
	}
	
	double nextXPos = xPos + xVel * deltaTime;


	if(nextXPos < 0)
	{
		gameMode->playerScoreRight++;
		PointScored();
		return;
	}
	
	if(nextXPos + rect.w > Screen::GetInstance()->GetWidth())
	{
		gameMode->playerScoreLeft++;
		PointScored();
		return;
	}
	xPos = nextXPos;
	rect.x = xPos;

	double nextYPos = yPos + yVel * deltaTime;
	if(nextYPos < 0 || nextYPos + rect.h > Screen::GetInstance()->GetHeight())
	{
		yVel = -yVel;
		nextYPos = yPos + yVel * deltaTime;
	}
	yPos = nextYPos;
	rect.y = yPos;

}

void Ball::Draw()
{
	scoreText.Render(Screen::GetInstance()->GetWidthCenter()-scoreText.GetWidth()/2,0);
	
	if(gameMode->GetGameState() != GameState::Playing)
	{
		return;
	}
	
	ballImg.Render(rect.x,rect.y);
}

void Ball::CheckCollisionWithPaddle(Paddle& paddle)
{
	const int xPaddle = paddle.GetPaddleRect().x;
	const int yPaddle = paddle.GetPaddleRect().y;
	const int wPaddle = paddle.GetPaddleRect().w;
	const int hPaddle = paddle.GetPaddleRect().h;
	
	if(paddle.GetPaddlePosition() == PaddlePosition::LEFT)
	{
		if(xPos < static_cast<double>(xPaddle) + wPaddle && xPos > static_cast<double>(xPaddle) + static_cast<double>(wPaddle)/3  && yPos+rect.h > yPaddle && yPos < static_cast<double>(yPaddle) + hPaddle)
		{
			xVel = -xVel;
		}
	} else
	{
		if(xPos+rect.w > xPaddle && xPos+rect.w < xPaddle + static_cast<double>(wPaddle) /3 && yPos+rect.h > yPaddle && yPos < static_cast<double>(yPaddle) + hPaddle)
		{
			xVel = -xVel;
		}
	}
	
	
}

void Ball::RandomizeStartingVelocity()
{
	std::random_device dev;
	std::mt19937_64 randGenerator(dev());
	const std::uniform_int_distribution<> intDistX(0,1);


	if(intDistX(randGenerator) == 0)
	{
		xVel = -BALL_SPEED;
	} else
	{
		xVel = BALL_SPEED;
	}
	const std::uniform_int_distribution<> intDistY(BALL_SPEED,BALL_SPEED);
	yVel = intDistY(randGenerator);
	if(intDistY(randGenerator))
	{
		yVel = -yVel;
	}
}
