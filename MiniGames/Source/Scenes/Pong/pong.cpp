﻿#include "Scenes/Pong/pong.h"

#include "inputManager.h"
#include "Scenes/SceneFactory.h"

constexpr int DIST_FROM_CENTER = 500;

void Pong::InitializeScene()
{
	Screen::GetInstance()->SetWindowTitle("Pong!!");
	gameState = GameState::StartGame;
	startGameImage.LoadFromFile("Images/PongStartUI.png");
	paddleLeft.InitializePaddle(DIST_FROM_CENTER);
	paddleRight.InitializePaddle(DIST_FROM_CENTER);
	ball.Initialize(this);
}

Uint32 StartGame(Uint32 interval, void* param)
{
	Pong* pongGame = static_cast<Pong*>(param);
	pongGame->SetGameState(GameState::Playing);
	pongGame->ResetStartTimer();
	return 0;
}

void Pong::HandleInput()
{
	if(startTimer == 0 && gameState != GameState::Playing && Input::IsPressed(Input::Keys::ENTER))
	{
		startTimer = SDL_AddTimer(3*1000,StartGame,this);
	}

	if(Input::IsPressed(Input::Keys::ESC))
	{
		if(gameState == GameState::EndGame)
		{
			SceneFactory::GetInstance()->SwapScenes(Scenes::MAIN_MENU);
			return;
		}
		gameState = GameState::EndGame;
	}
	
	paddleLeft.HandleInput();
	paddleRight.HandleInput();
}

void Pong::Update(const double deltaTime)
{
	paddleLeft.Update(deltaTime);
	paddleRight.Update(deltaTime);
	ball.Update(deltaTime);
	ball.CheckCollisionWithPaddle(paddleLeft);
	ball.CheckCollisionWithPaddle(paddleRight);
}

void Pong::Draw()
{
	startGameImage.Render((Screen::GetInstance()->GetWidth() - startGameImage.GetWidth()) /2,(Screen::GetInstance()->GetHeight() - startGameImage.GetHeight()) /2);
	paddleLeft.Draw();
	paddleRight.Draw();
	ball.Draw();
}

void Pong::CloseScene()
{
	startGameImage.Free();
}

GameState Pong::GetGameState() const
{
	return gameState;
}

void Pong::SetGameState(const GameState state)
{
	gameState = state;
}
