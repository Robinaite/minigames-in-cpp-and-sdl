Minigames in C++ and SDL is a small project of mine where I program some small games based on the same framework I created.

The most important functions are:
- Simple version of what are "Scenes" or "Worlds".
- Global Input access.
- FMOD Studio Audio Engine integration

The idea was to easily allow different games being in the same .exe file!

The objective was to understand better how to make proper use of C++, as well as, implementing respective design patterns using it. Some patterns used where, Factory and Singleton.

The code is accessible under MIT License, so feel free to use it(or not) 😜
